import CollaboratorRepository from "@/domain/CollaboratorRepository";
import {
    Collaborator,
    CollaboratorFactory,
    CollaboratorId,
    CollaboratorIdFactory,
    StatusFactory,
    UsernameFactory
} from "@/domain/Collaborator";

const localStorage = window.localStorage;
const STORAGE_KEY = 'collaborators';

const getFromStorage = (): [] => {
    return JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]');
}

const setToStorage = (data: []) => {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(data));
}

const collaboratorMapper = (data: any): Collaborator => {
    return CollaboratorFactory.new(
        CollaboratorIdFactory.fromString(data.id),
        UsernameFactory.fromString(data.username),
        StatusFactory.fromString(data.status)
    );
}

export default {
    create(): CollaboratorRepository {
        return {
            get(collaboratorId: CollaboratorId): Promise<Collaborator> {
                return new Promise((resolve, reject) => {
                    const items = getFromStorage();

                    const result = items.map(collaboratorMapper).find((item: Collaborator) => {
                        return item.id.equalsTo(collaboratorId);
                    });

                    result ? resolve(result) : reject(`No collaborator found with id ${collaboratorId.toString()}`);
                });
            },
            save(collaborator: Collaborator): Promise<void> {
                if (collaborator.username.toString() === 'tutu') {
                    return Promise.reject({
                        violations: [
                            {
                                propertyPath: 'username',
                                message: 'No tutu allowed :('
                            }
                        ]
                    });
                }

                const items = getFromStorage();

                // @ts-ignore
                const existingItem = items.find((item) => ( item.id === collaborator.id.toString() ));

                if (existingItem) {
                    // @ts-ignore
                    existingItem.status = collaborator.status.toString();
                } else {
                    items.push({
                        // @ts-ignore
                        id: collaborator.id.toString(),
                        // @ts-ignore
                        username: collaborator.username.toString(),
                        // @ts-ignore
                        status: collaborator.status.toString()
                    });
                }

                setToStorage(items);

                return Promise.resolve();
            },
            findAll(): Promise<Collaborator[]> {
                const items = getFromStorage();

                return Promise.resolve(items.map(collaboratorMapper));
            }
        };
    }
};
