import {v4 as uuidv4 } from "uuid";
const localStorage = window.localStorage;

export default {
    generate() {
        localStorage.setItem('collaborators', JSON.stringify([
            {
                id: uuidv4(),
                username: 'Roberto Carlos',
                status: 'activated'
            },
            {
                id: uuidv4(),
                username: 'Michèle Duchemin',
                status: 'activated'
            },
            {
                id: uuidv4(),
                username: 'Ginette Roussin',
                status: 'activated'
            }
        ]))
    }
};
