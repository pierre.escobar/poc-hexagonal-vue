import {CollaboratorsListingService} from "@/domain/CollaboratorsListingService";
import {COLLABORATORS_RETRIEVED} from "@/infrastructure/store/MutationConstants";
import {Collaborator} from "@/domain/Collaborator";
import {DeactivationService} from "@/domain/DeactivationService";
import {CollaboratorRegistrationService} from "@/domain/CollaboratorRegistrationService";
import {v4 as uuidv4 } from "uuid";

export default {
    create(
        collaboratorListingService: CollaboratorsListingService,
        deactivationService: DeactivationService,
        collaboratorRegistrationService: CollaboratorRegistrationService
    ) {
        return {
            state: {
                collaborators: []
            },
            mutations: {
                [COLLABORATORS_RETRIEVED]: (state: any, payload: Collaborator[]) => {
                    state.collaborators = payload;
                }
            },
            actions: {
                async findCollaborators(context: any) {
                    const results = await collaboratorListingService.findCollaborators();
                    context.commit(COLLABORATORS_RETRIEVED, results);
                },
                async requestDeactivation(context: any, payload: any) {
                    await deactivationService.requestDeactivation({
                        id: payload.id
                    });
                    context.dispatch('findCollaborators');
                },
                async registerCollaborator(context: any, payload: any) {
                    await collaboratorRegistrationService.register({
                        id: uuidv4(),
                        username: payload.username
                    });
                    context.dispatch('findCollaborators');
                }
            }
        }
    }
};
