import store from '@/store';
import Vue from 'vue'
import App from './App.vue'
import LocalStorageCollaboratorFixtures from "@/infrastructure/repository/LocalStorageCollaboratorFixtures";

Vue.config.productionTip = false

LocalStorageCollaboratorFixtures.generate();

new Vue({
  render: h => h(App),
  store,
}).$mount('#app')
