import CollaboratorRepository from "@/domain/CollaboratorRepository";
import {Collaborator, CollaboratorIdFactory} from "@/domain/Collaborator";

export type DeactivateCollaborator = {
    readonly id: string
}

export type DeactivationService = {
    requestDeactivation(command: DeactivateCollaborator): Promise<void>;
}

export const DeactivationServiceFactory = {
    create(collaboratorRepository: CollaboratorRepository): DeactivationService {
        return {
            async requestDeactivation(command: DeactivateCollaborator): Promise<void> {
                const collaborator = await collaboratorRepository.get(CollaboratorIdFactory.fromString(command.id));

                collaborator.requestDeactivation();

                return collaboratorRepository.save(collaborator);
            }
        };
    }
}
