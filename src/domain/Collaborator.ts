const ACTIVATED_STATUS = 'activated';
const DEACTIVATION_REQUESTED_STATUS = 'deactivation_requested';
const DEACTIVATED_STATUS = 'deactivated';

export type CollaboratorId = {
    toString(): string;
    equalsTo(collaboratorId: CollaboratorId): boolean;
};

export const CollaboratorIdFactory = {
    fromString(id: string): CollaboratorId {
        return {
            toString() {
                return id;
            },
            equalsTo(collaboratorId: CollaboratorId): boolean {
                return collaboratorId.toString() === id;
            }
        }
    }
};

export type Username = {
    toString(): string;
    equalsTo(username: Username): boolean;
};

export const UsernameFactory = {
    fromString(value: string): Username {
        return {
            toString() {
                return value;
            },
            equalsTo(username: Username): boolean {
                return username.toString() === value;
            }
        }
    }
};

export type Status = {
    toString(): string;
    equalsTo(status: Status): boolean;
}

export const StatusFactory = {
    fromString(value: string): Status {
        const statusList = [ACTIVATED_STATUS, DEACTIVATION_REQUESTED_STATUS, DEACTIVATED_STATUS];

        if (!statusList.find((item) => (item === value))) {
            throw new Error(`Invalid status ${value}`);
        }

        return {
            toString(): string {
                return value;
            },
            equalsTo(status: Status): boolean {
                return status.toString() === value;
            }
        }
    }
};

export type Collaborator = {
    readonly id: CollaboratorId,
    readonly username: Username,
    status: Status
    requestDeactivation(): void;
    activated(): boolean;
};

export const CollaboratorFactory = {
    new(collaboratorId: CollaboratorId, username: Username, status: Status): Collaborator {
        return {
            id: collaboratorId,
            username,
            status,
            requestDeactivation() {
                const newStatus = StatusFactory.fromString(DEACTIVATION_REQUESTED_STATUS);
                if (this.status.toString() !== ACTIVATED_STATUS) {
                    throw new Error(`The collaborator status ${status.toString()} does not allow deactivation.`);
                }

                this.status = newStatus;
            },
            activated(): boolean {
                return status.toString() === ACTIVATED_STATUS;
            }
        };
    },
    register(collaboratorId: CollaboratorId, username: Username) {
        return this.new(collaboratorId, username, StatusFactory.fromString(ACTIVATED_STATUS));
    }
}
