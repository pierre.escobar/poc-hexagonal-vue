import {Collaborator, CollaboratorId} from "@/domain/Collaborator";

type Violation = {
    propertyPath: string,
    message: string
};

export type ExternalValidationError = {
    violations: Violation[]
}

type CollaboratorRepository = {
    get(collaboratorId: CollaboratorId): Promise<Collaborator>;
    save(collaborator: Collaborator): Promise<void>;
    findAll(): Promise<Collaborator[]>;
};

export default CollaboratorRepository;
