import CollaboratorRepository from "@/domain/CollaboratorRepository";
import {CollaboratorFactory, CollaboratorIdFactory, StatusFactory, UsernameFactory} from "@/domain/Collaborator";

export type RegisterCollaborator = {
    id: string;
    username: string;
}

export type CollaboratorRegistrationService = {
    register(command: RegisterCollaborator): Promise<void>;
}

export const CollaboratorRegistrationServiceFactory = {
    create(collaboratorRepository: CollaboratorRepository): CollaboratorRegistrationService {
        return {
            async register(command: RegisterCollaborator): Promise<void> {
                const collaborator = CollaboratorFactory.register(
                    CollaboratorIdFactory.fromString(command.id),
                    UsernameFactory.fromString(command.username)
                );

                return collaboratorRepository.save(collaborator);
            }
        }
    }
};
