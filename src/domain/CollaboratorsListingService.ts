import CollaboratorRepository from "@/domain/CollaboratorRepository";
import {Collaborator} from "@/domain/Collaborator";

export type CollaboratorsListingService = {
    findCollaborators(): Promise<Collaborator[]>
}

export const CollaboratorListingServiceFactory = {
    create(collaboratorRepository: CollaboratorRepository): CollaboratorsListingService {
        return {
            async findCollaborators(): Promise<Collaborator[]> {
                return await collaboratorRepository.findAll();
            }
        };
    }
}
