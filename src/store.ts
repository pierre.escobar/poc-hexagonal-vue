import Vuex from "vuex";
import Vue from 'vue';
import VuexCollaboratorListingServiceAdapter from "@/infrastructure/store/VuexCollaboratorListingServiceAdapter";
import {CollaboratorListingServiceFactory} from "@/domain/CollaboratorsListingService";
import LocalStorageCollaboratorRepository from "@/infrastructure/repository/LocalStorageCollaboratorRepository";
import {DeactivationServiceFactory} from "@/domain/DeactivationService";
import {CollaboratorRegistrationServiceFactory} from "@/domain/CollaboratorRegistrationService";

Vue.use(Vuex);

const collaboratorRepository = LocalStorageCollaboratorRepository.create();
const collaboratorListingService = CollaboratorListingServiceFactory.create(collaboratorRepository);
const deactivationService = DeactivationServiceFactory.create(collaboratorRepository);
const collaboratorRegistrationService = CollaboratorRegistrationServiceFactory.create(collaboratorRepository);
const collaborators = VuexCollaboratorListingServiceAdapter.create(
    collaboratorListingService,
    deactivationService,
    collaboratorRegistrationService
);

export default new Vuex.Store({
    modules: {
        collaborators
    }
});
